package com.indobytes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.indobytes.ridehubdriver.R;

public class Register_Webview extends AppCompatActivity {


    WebView webView;
    ImageView ham_home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register__webview);

        webView = (WebView) findViewById(R.id.webView);
        ham_home = (ImageView) findViewById(R.id.ham_home);

        ham_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             onBackPressed();
                /*Intent intent = new Intent(Register_Webview.this, HomePage.class);
                startActivity(intent);*/
            }
        });

        String postUrl = getIntent().getStringExtra("base_url");
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(postUrl);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
