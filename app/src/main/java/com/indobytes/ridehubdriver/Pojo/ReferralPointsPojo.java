package com.indobytes.ridehubdriver.Pojo;

/**
 * Created by user14 on 9/24/2015.
 */
public class ReferralPointsPojo {
    private String refer_from;
    private String refer_to;
    private String points;
    private String referrer_name;
    private String type;
    private String ride_id;
    private String trans_type;
    private String level;
    private String date;


    public String getRefer_from() {
        return refer_from;
    }

    public void setRefer_from(String refer_from) {
        this.refer_from = refer_from;
    }



    public String getRefer_to() {
        return refer_to;
    }

    public void setRefer_to(String refer_to) {
        this.refer_to = refer_to;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getReferrer_name() {
        return referrer_name;
    }

    public void setReferrer_name(String referrer_name) {
        this.referrer_name = referrer_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRide_id() {
        return ride_id;
    }

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }

    public String getTrans_type() {
        return trans_type;
    }

    public void setTrans_type(String trans_type) {
        this.trans_type = trans_type;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
