package com.indobytes.ridehubdriver;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Hockeyapp.FragmentHockeyApp;
import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.indobytes.ridehubdriver.Utils.ConnectionDetector;
import com.indobytes.ridehubdriver.Utils.CurrencySymbolConverter;
import com.indobytes.ridehubdriver.Utils.SessionManager;
import com.indobytes.ridehubdriver.widgets.CustomTextView;
import com.indobytes.ridehubdriver.widgets.PkDialog;
import com.special.ResideMenu.ResideMenu;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


/**
 * Created by Prem Kumar and Anitha on 10/13/2015.
 */
public class InviteAndEarn extends FragmentHockeyApp implements View.OnClickListener {
    final int PERMISSION_REQUEST_CODE = 111;
    LinearLayout Rl_whatsApp, Rl_messenger, Rl_sms, Rl_email, Rl_twitter, Rl_facebook;
    String sCurrencySymbol = "";
    CustomTextView tv_invite_now, tv_shareride,ll_shareinvitecode;
    private RelativeLayout back;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private TextView Tv_friends_earn, Tv_you_earn, Tv_referral_code;
    //private RelativeLayout Rl_whatsApp, Rl_messenger, Rl_sms, Rl_email, Rl_twitter, Rl_facebook;
    private ServiceRequest mRequest;
    private String UserID = "";
    private boolean isdataPresent = false;
    private String Sstatus = "", friend_earn_amount = "", you_earn_amount = "", friends_rides = "", ScurrencyCode = "", referral_code = "", sShareLink = "";
    private String package_name = "com.indobytes.ridehubdriver";

    private View parentView;
    private ActionBar actionBar;
    private ResideMenu resideMenu;




    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.freeride_ride_hub, container, false);
        ActionBarActivity actionBarActivity = (ActionBarActivity) getActivity();
        actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.hide();
        initialize(parentView);

        parentView.findViewById(R.id.ham_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(resideMenu != null ){
                    resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
                }
            }
        });

        setUpViews();
        return parentView;
    }

    private void setUpViews() {
        NavigationDrawer parentActivity = (NavigationDrawer) getActivity();
        resideMenu = parentActivity.getResideMenu();
    }


    private void initialize(View rootview) {
        session = new SessionManager(getActivity());
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

      //  back = (RelativeLayout) rootview.findViewById(R.id.invite_earn_header_back_layout);
       // Rl_whatsApp = (LinearLayout) rootview.findViewById(R.id.invite_earn_whatsapp_layout);
        //Rl_messenger = (LinearLayout) rootview.findViewById(R.id.invite_earn_messenger_layout);
        //Rl_sms = (LinearLayout) rootview.findViewById(R.id.invite_earn_sms_layout);
        //Rl_email = (LinearLayout) rootview.findViewById(R.id.invite_earn_email_layout);
        //Rl_twitter = (LinearLayout) rootview.findViewById(R.id.invite_earn_twitter_layout);
       // Rl_facebook = (LinearLayout) rootview.findViewById(R.id.invite_earn_facebook_layout);
        Tv_friends_earn = (TextView) rootview.findViewById(R.id.invite_earn_friend_earn_textview);
        Tv_you_earn = (TextView) rootview.findViewById(R.id.invite_earn_you_earn_textview);
        Tv_referral_code = (TextView) rootview.findViewById(R.id.invite_earn_referral_code_textview);
        //tv_invite_now = (CustomTextView) rootview.findViewById(R.id.tv_invite_now);
        ll_shareinvitecode = (CustomTextView) rootview.findViewById(R.id.ll_shareinvitecode);
        tv_shareride = (CustomTextView) rootview.findViewById(R.id.tv_shareride);


        SpannableString relative = new SpannableString("Share the Ride Hub love and give friends free rides to try Ride, Worth up to $10 each!");
        Log.e("length_relative", "" + relative.length());
        relative.setSpan(new RelativeSizeSpan(2f), relative.length() - 9, relative.length() - 6, 0);
        relative.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.free_rides_color)), relative.length() - 9, relative.length() - 6, 0);// set color
        /*relative.setSpan(new RelativeSizeSpan(0.5f), relative.length() - 14, relative.length() - 7,
                0);*/
        tv_shareride.setText(relative);


       // Rl_whatsApp.setOnClickListener(this);
       // Rl_messenger.setOnClickListener(this);
       // Rl_sms.setOnClickListener(this);
        //Rl_email.setOnClickListener(this);
        //Rl_twitter.setOnClickListener(this);
       // Rl_facebook.setOnClickListener(this);
        //tv_invite_now.setOnClickListener(this);
        ll_shareinvitecode.setOnClickListener(this);

        ll_shareinvitecode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " to ride free.Enjoy! " + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en";
                whatsApp_sendMsg(text);

            }
        });

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_DRIVERID);

        if (isInternetPresent) {
              displayInvite_Request(ServiceConstant.inviteandEarn_Url);
            // PostRequest(ServiceConstant.referralpoints_url);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }

    }

    @Override
    public void onClick(View v) {
        if (isdataPresent) {
            String text = getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " to ride free.Enjoy!" + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en";
            /*if (v == tv_invite_now) {
                whatsApp_sendMsg(text);
            } else if (v == Rl_messenger) {
                messenger_sendMsg(text);
            } else if (v == Rl_sms) {
                sms_sendMsg(text);
            } else if (v == Rl_email) {
                sendEmail(text);
            } else if (v == Rl_twitter) {
                String twitter_text = getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " to ride free.Enjoy!" + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en";
                Uri imageUri = null;
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(getActivity().getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.invite_and_earn_car_new), null, null));
                } catch (NullPointerException e) {
                }
                shareTwitter(twitter_text, imageUri);
            } else if (v == Rl_facebook) {
                String facebook_text = getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " to ride free.Enjoy!" + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en";
                Uri imageUri = null;
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(getActivity().getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.invite_and_earn_car_new), null, null));
                } catch (NullPointerException e) {
                }

                if (sShareLink.length() > 0) {
                    shareFacebookLink(sShareLink);
                } else {
                    shareFacebook(facebook_text, imageUri);
                }

            }*/
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_problem_server));
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    //--------Sending message on WhatsApp Method------
    private void whatsApp_sendMsg(String text) {
        PackageManager pm = getActivity().getPackageManager();
        /* try {
           Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);  //Check if package exists or not. If not then codein catch block will be called
            //waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));

        } catch (PackageManager.NameNotFoundException e) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_whatsApp_not_installed));
        }*/
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, text);
            sendIntent.setType("text/plain");
            Intent.createChooser(sendIntent, "Share via");
            startActivity(sendIntent);
        } catch (Exception e) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_whatsApp_not_installed));
        }


    }

    //--------Sending message on Facebook Messenger Method------
    private void messenger_sendMsg(String text) {
        PackageManager pm = getActivity().getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.facebook.orca", PackageManager.GET_META_DATA);  //Check if package exists or not. If not then codein catch block will be called
            waIntent.setPackage("com.facebook.orca");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_messenger_not_installed));
        }
    }

    //--------Sending message on SMS Method------
    private void sms_sendMsg(String text) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            /*if (!checkSmsPermission()) {
               // requestPermission();
            } else {*/
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(getActivity()); //Need to change the build to API 19
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, text);
            if (defaultSmsPackageName != null) {
                sendIntent.setPackage(defaultSmsPackageName);
            }
            startActivity(sendIntent);
            //}
        } else {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.putExtra("sms_body", text);
            sendIntent.setType("vnd.android-dir/mms-sms");
            startActivity(sendIntent);
        }
    }


    //----------Sending message on Email Method--------
    protected void sendEmail(String text) {
        String[] TO = {""};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Rideys app Invitation");
        emailIntent.putExtra(Intent.EXTRA_TEXT, text);
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_email_not_installed));
        }
    }


    //----------Share Image and Text on Twitter Method--------
    protected void shareTwitter(String text, Uri image) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_STREAM, image);
        intent.setType("image/jpeg");
        intent.setPackage("com.twitter.android");

        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_twitter_not_installed));
        }
    }

    //----------Share Link on Method--------
    private void shareFacebookLink(String link) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, link);
        Intent pacakage1 = getActivity().getPackageManager().getLaunchIntentForPackage("com.facebook.orca");
        Intent pacakage2 = getActivity().getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
        Intent pacakage3 = getActivity().getPackageManager().getLaunchIntentForPackage("com.example.facebook");
        Intent pacakage4 = getActivity().getPackageManager().getLaunchIntentForPackage("com.facebook.android");
        if (pacakage1 != null) {
            intent.setPackage("com.facebook.orca");
        } else if (pacakage2 != null) {
            intent.setPackage("com.facebook.katana");
        } else if (pacakage3 != null) {
            intent.setPackage("com.facebook.facebook");
        } else if (pacakage4 != null) {
            intent.setPackage("com.facebook.android");
        } else {
            intent.setPackage("com.facebook.orca");
        }

        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_facebook_not_installed));
        }
    }

    //----------Share Image and Text on Facebook Method--------
    protected void shareFacebook(String text, Uri image) {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        //intent.setType("text/plain");
        intent.setType("image/*");
        //intent.putExtra(Intent.EXTRA_TEXT, "http://project.dectar.com/fortaxi/rider/signup?ref=QVJPQ0tJQQ==");
        intent.putExtra(Intent.EXTRA_STREAM, image);
        Intent pacakage1 = getActivity().getPackageManager().getLaunchIntentForPackage("com.facebook.orca");
        Intent pacakage2 = getActivity().getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
        Intent pacakage3 = getActivity().getPackageManager().getLaunchIntentForPackage("com.example.facebook");
        Intent pacakage4 = getActivity().getPackageManager().getLaunchIntentForPackage("com.facebook.android");
        if (pacakage1 != null) {
            intent.setPackage("com.facebook.orca");
        } else if (pacakage2 != null) {
            intent.setPackage("com.facebook.katana");
        } else if (pacakage3 != null) {
            intent.setPackage("com.facebook.facebook");
        } else if (pacakage4 != null) {
            intent.setPackage("com.facebook.android");
        } else {
            intent.setPackage("com.facebook.orca");
        }

        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_facebook_not_installed));
        }
    }


    //-----------------------Display Invite Amount Post Request-----------------
    private void displayInvite_Request(String Url) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(InviteAndEarn.this.getResources().getString(R.string.action_pleasewait));

        System.out.println("-------------displayInvite_Request Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", UserID);

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------displayInvite_Request Response----------------" + response);

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (object.length() > 0) {
                        JSONObject response_Object = object.getJSONObject("response");
                        if (response_Object.length() > 0) {
                            JSONObject detail_object = response_Object.getJSONObject("details");
                            if (detail_object.length() > 0) {
                                friend_earn_amount = detail_object.getString("friends_earn_amount");
                                you_earn_amount = detail_object.getString("your_earn_amount");
                                friends_rides = detail_object.getString("your_earn");
                                referral_code = detail_object.getString("referral_code");
                                ScurrencyCode = detail_object.getString("currency");
                                sShareLink = detail_object.getString("url");

                                isdataPresent = true;
                            } else {
                                isdataPresent = false;
                            }
                        } else {
                            isdataPresent = false;
                        }
                    } else {
                        isdataPresent = false;
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isdataPresent) {
                    sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(ScurrencyCode);

                    String earn_text = "<font color=#1d3b6f>" + getResources().getString(R.string.invite_earn_label_friends_earn) + "</font>" + "<font color=#ef5e27>" + " " + sCurrencySymbol + "" + you_earn_amount + "</font>";

                    Tv_friends_earn.setText(Html.fromHtml(earn_text));
                    // Tv_friends_earn.setText(getResources().getString(R.string.invite_earn_label_friends_earn) + " " + sCurrencySymbol + "" + friend_earn_amount);
                    String code_text = "<font color=#1d3b6f>" + getResources().getString(R.string.invite_earn_label_friends_ride) + ", " + getResources().getString(R.string.invite_earn_label_friend_ride) + "</font>" + "<font color=#ef5e27>" + sCurrencySymbol + "" + you_earn_amount + "</font>";
                    Tv_you_earn.setText(Html.fromHtml(code_text));
                    //Tv_you_earn.setText(getResources().getString(R.string.invite_earn_label_friends_ride) + "," + getResources().getString(R.string.invite_earn_label_friend_ride) + " " + sCurrencySymbol + "" + you_earn_amount);
                    Tv_referral_code.setText(referral_code);
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


   /* private boolean checkSmsPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }*/

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    String text = getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " " + getResources().getString(R.string.invite_earn_label_share_messgae3);

                    String defaultSmsPackageName = null; //Need to change the build to API 19
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(getActivity());
                    }

                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                    if (defaultSmsPackageName != null) {
                        sendIntent.setPackage(defaultSmsPackageName);
                    }
                    startActivity(sendIntent);
                } else {
                }
                break;
        }
    }


    //-----------------Move Back on pressed phone back button------------------
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            // nothin
            return true;
        }
        return false;
    }

}