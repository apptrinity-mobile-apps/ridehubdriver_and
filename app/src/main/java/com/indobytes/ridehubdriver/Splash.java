package com.indobytes.ridehubdriver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.indobytes.ridehubdriver.Utils.ConnectionDetector;
import com.indobytes.ridehubdriver.Utils.SessionManager;
import com.indobytes.ridehubdriver.Utils.SessionManager_Applaunch;
import com.indobytes.ridehubdriver.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by user88 on 6/3/2016.
 */
public class Splash extends Activity {


    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    Context context;
    private ServiceRequest mRequest;
    String Strstatus = "",Str_phonemasking_status="";
    private ProgressBar progress;

    String sPendingRideId="",sRatingStatus="";
    private boolean isAppInfoAvailable = false;

    private SessionManager_Applaunch session_launch;

    private SessionManager session;
    private String driver_id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        context = getApplicationContext();
        cd = new ConnectionDetector(Splash.this);
        isInternetPresent = cd.isConnectingToInternet();
        session =  new SessionManager(Splash.this);

        session_launch  = new SessionManager_Applaunch(Splash.this);
        progress = (ProgressBar)findViewById(R.id.progress_splash);

        cd = new ConnectionDetector(Splash.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> user = session.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);

        if (isInternetPresent) {
            postRequest_applaunch(ServiceConstant.app_launching_url);
            System.out.println("applaunch------------------" + ServiceConstant.app_launching_url);
        } else {
            Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
        }

        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    Intent intent = new Intent(Splash.this,HomePage.class);
                    startActivity(intent);
                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }

    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(Splash.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //-----------------------Post Request-----------------
/*    private void postRequest_applaunch(String Url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        mRequest = new ServiceRequest(SplashPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                System.out.println("applaunch-----------------" + response);
                Log.e("launch",response);
                try{
                    JSONObject object = new JSONObject(response);
                    Strstatus  = object.getString("status");

                    if (Strstatus.equalsIgnoreCase("1")){

                        JSONObject jobject = object.getJSONObject("response");
                        JSONObject object_launch = jobject.getJSONObject("info");

                        Str_phonemasking_status = object_launch.getString("phone_masking_status");
                    }

                    if (Strstatus.equalsIgnoreCase("1")){
                        session_launch.createSessionPhoneMask(Str_phonemasking_status);
                    }else{
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {

            }

        });
    }*/



    //-----------------------App Information Post Request-----------------
    private void postRequest_applaunch(String Url) {

        System.out.println("-------------Splash App Information Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "driver");
        jsonParams.put("id", driver_id);
        mRequest = new ServiceRequest(Splash.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Splash App Information Response----------------" + response);


                String Str_status = "", sContact_mail = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                                sFacebookId = info_object.getString("facebook_app_id");
                                sGooglePlusId = info_object.getString("google_plus_app_id");
                                sPhoneMasking = info_object.getString("phone_masking_status");

                                isAppInfoAvailable = true;
                            } else {
                                isAppInfoAvailable = false;
                            }

                            sPendingRideId= response_object.getString("pending_rideid");
                            sRatingStatus= response_object.getString("ride_status");

                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }

                    if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {

                        session = new SessionManager(Splash.this);
                        if (session.isLoggedIn()) {
                            Intent i = new Intent(Splash.this, DashBoardDriver.class);
                            startActivity(i);
                            finish();
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        } else {
                            Intent i = new Intent(Splash.this, HomePage.class);
                            startActivity(i);
                            finish();
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }
                    } else {

                        Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.fetchdatatoast));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

            }
        });
    }

}
