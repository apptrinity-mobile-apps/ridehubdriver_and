package com.indobytes.ridehubdriver.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indobytes.ridehubdriver.Pojo.TripSummaryPojo;
import com.indobytes.ridehubdriver.R;

import java.util.ArrayList;

/**
 * Created by user88 on 10/23/2015.
 */
public class TripSummeryAdapter extends BaseAdapter {
    private ArrayList<TripSummaryPojo> data;
    private LayoutInflater mInflater;
    private Activity context;
    private String check;

    public TripSummeryAdapter(Activity c, ArrayList<TripSummaryPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView trip_summery_tvaddress;
        private TextView trip_summery_tvdate;
        private ImageView time_imageview;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.trip_summary_list, parent, false);
            holder = new ViewHolder();
            holder.trip_summery_tvaddress = (TextView) view.findViewById(R.id.trip_summary_list_address);
            holder.trip_summery_tvdate = (TextView) view.findViewById(R.id.trip_summary_list_date_and_time);
            holder.time_imageview = (ImageView) view.findViewById(R.id.time_imageview);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        LinearLayout recent_main_layout = (LinearLayout) view.findViewById(R.id.recent_main_layout);
       /* if (position % 2 == 1) {*/
            recent_main_layout.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.trip_summery_tvaddress.setTextColor(context.getResources().getColor(R.color.app_color));
            holder.trip_summery_tvdate.setTextColor(context.getResources().getColor(R.color.light_grey));
           // holder.time_imageview.setImageDrawable(context.getResources().getDrawable(R.drawable.your_trip_blue));
        /*} else {
            recent_main_layout.setBackgroundColor(context.getResources().getColor(R.color.app_color));
            holder.trip_summery_tvaddress.setTextColor(context.getResources().getColor(R.color.white));
            holder.trip_summery_tvdate.setTextColor(context.getResources().getColor(R.color.blue_ridehub));
            holder.time_imageview.setImageDrawable(context.getResources().getDrawable(R.drawable.carwhite_tripsummary_ridehub));
        }*/

        holder.trip_summery_tvaddress.setText(data.get(position).getpickup());
        holder.trip_summery_tvdate.setText(data.get(position).getdatetime());
        return view;
    }


}
