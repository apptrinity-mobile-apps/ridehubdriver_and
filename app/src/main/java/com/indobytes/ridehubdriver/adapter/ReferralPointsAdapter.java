package com.indobytes.ridehubdriver.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.indobytes.ridehubdriver.Pojo.ReferralPointsPojo;
import com.indobytes.ridehubdriver.R;
import com.indobytes.ridehubdriver.widgets.CustomTextView;

import java.util.ArrayList;

/**
 * Created by user14 on 9/22/2015.
 */
public class ReferralPointsAdapter extends BaseAdapter {

    private ArrayList<ReferralPointsPojo> data;
    private LayoutInflater mInflater;
    private Activity context;
    private String check;

    public ReferralPointsAdapter(Activity c, ArrayList<ReferralPointsPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private CustomTextView tv_refer_name;
        private CustomTextView tv_refer_transaction_type;
        private CustomTextView tv_refer_rideid;
        private CustomTextView tv_refer_level;
        private CustomTextView tv_refer_points;
        private CustomTextView tv_datereferal;
        private CustomTextView tv_refer_type;
        private ImageView track_your_ride_driverimage;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        String data1 = " ";
        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item_refer_points, parent, false);
            holder = new ViewHolder();
            holder.tv_refer_name = (CustomTextView) view.findViewById(R.id.tv_refer_name);
           // holder.tv_refer_transaction_type = (CustomTextView) view.findViewById(R.id.tv_refer_transaction_type);
            holder.tv_refer_rideid = (CustomTextView) view.findViewById(R.id.tv_refer_rideid);
            holder.tv_refer_level = (CustomTextView) view.findViewById(R.id.tv_refer_level);
            holder.tv_refer_points = (CustomTextView) view.findViewById(R.id.tv_refer_points);
            holder.tv_datereferal = (CustomTextView) view.findViewById(R.id.tv_datereferal);
            holder.tv_refer_type = (CustomTextView) view.findViewById(R.id.tv_refer_type);
            holder.track_your_ride_driverimage = (ImageView) view.findViewById(R.id.track_your_ride_driverimage);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        Log.e("POINTS_REFERRAL",data.get(position).getPoints());
        holder.tv_refer_name.setText(data.get(position).getReferrer_name());
       // holder.tv_refer_transaction_type.setText(data.get(position).getTrans_type() +"-");
        holder.tv_refer_rideid.setText("Refer Id : " +data.get(position).getRide_id());
        holder.tv_refer_level.setText("Refer Level : " +data.get(position).getLevel());
        holder.tv_refer_points.setText(data.get(position).getTrans_type() + " - " +data.get(position).getPoints());
        holder.tv_datereferal.setText("Date : " +data.get(position).getDate());
        holder.tv_refer_type.setText("Refer Type : " +data.get(position).getType());

        /*if(data.get(position).getType().equals("driver")){
            holder.track_your_ride_driverimage.setImageDrawable(context.getResources().getDrawable(R.drawable.referral_driver));
        }*/

       /* if(data.get(position).getTrans_type().equals("CR")){
            holder.tv_refer_transaction_type.setText(data.get(position).getTrans_type());
            holder.tv_refer_transaction_type.setTextColor(context.getResources().getColor(R.color.green_alert_dialog_text));
        }else {
            holder.tv_refer_transaction_type.setText(data.get(position).getTrans_type());
            holder.tv_refer_transaction_type.setTextColor(context.getResources().getColor(R.color.alert_red));
        }*/

        return view;
    }


}
