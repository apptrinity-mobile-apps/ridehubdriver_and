package com.indobytes.ridehubdriver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.Hockeyapp.ActivityHockeyApp;
import com.app.service.ServiceConstant;
import com.app.xmpp.ChatingService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.indobytes.ridehubdriver.Utils.GPSTracker;
import com.indobytes.ridehubdriver.Utils.SessionManager;

import org.jsoup.Jsoup;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


public class HomePage extends ActivityHockeyApp implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private Button mSignIn;
    private Button mRegister;
    private SessionManager session;
    private GPSTracker gps;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private PendingResult<LocationSettingsResult> result;
    private final static int REQUEST_LOCATION = 199;
    private String package_name = "com.indobytes.ridehubdriver";
    private final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2313;
    Dialog dialog;
    private static final int REQUEST_SELFIE = 123;
    private static String[] PERMISSIONS_SELFIE = {Manifest.permission.READ_PHONE_STATE};

    public static  HomePage homePage;


   /* private void onRequestRunTimePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }*/
   final int PERMISSION_REQUEST_CODE = 111;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.homepage);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.homepage);
        } else {
            setContentView(R.layout.homepage);
        }

        handleSSLHandshake();

        homePage = HomePage.this;

       // onRequestRunTimePermission();
        mSignIn = (Button) findViewById(R.id.btn_signin);
        mRegister = (Button) findViewById(R.id.btn_register);
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here
        }
        web_update();

        Log.d("update", "stutd==="+web_update());
        if(web_update()){
            Alert("Update Available","Do you want to update.?" );
        }
        gps = new GPSTracker(getApplicationContext());
        mGoogleApiClient = new GoogleApiClient.Builder(HomePage.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();
        if (gps.isgpsenabled() && gps.canGetLocation()) {
            //do nothing
        } else {
            enableGpsService();
        }
        try {

        } catch (Exception e) {

        }
        session = new SessionManager(HomePage.this);
        session.createSessionOnline("0");
        session.setRequestCount(0);


        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission()) {
                requestPermission();
            } else {
                if (session.isLoggedIn()) {
                    ChatingService.startDriverAction(HomePage.this);
                    Intent i = new Intent(getApplicationContext(), NavigationDrawer.class);
                    startActivity(i);
                    finish();
                }
            }
        } else {
            if (session.isLoggedIn()) {
                ChatingService.startDriverAction(HomePage.this);
                Intent i = new Intent(getApplicationContext(), NavigationDrawer.class);
                startActivity(i);
                finish();
            }
        }

        mSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), LoginPage.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }

        });
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ///chaitanya
                if ((ContextCompat.checkSelfPermission(HomePage.this, Manifest.permission.READ_PHONE_STATE)
                        != PackageManager.PERMISSION_GRANTED)) {
                    requestCameraAndExternalPermission();
                }else{
                    TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                    String android_device_id = telephonyManager.getDeviceId();
                    Log.d("IMEI NUmber",""+telephonyManager.getDeviceId());//+"?device_id="+android_device_id
                   /* String s = (ServiceConstant.Register_URL+"?device_id="+android_device_id);
                    Intent intent = new Intent(HomePage.this, Register_Webview.class);
                    intent.putExtra("base_url",s);
                    startActivity(intent);*/

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ServiceConstant.Register_URL+"?device_id="+android_device_id));
                    startActivity(browserIntent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }

            }
        });

    }


    private long value(String string) {
        string = string.trim();
        if (string.contains(".")) {
            final int index = string.lastIndexOf(".");
            return value(string.substring(0, index)) * 100 + value(string.substring(index + 1));
        } else {
            return Long.valueOf(string);
        }
    }

    //--------------------------code to update checker------------------
    private boolean web_update() {
        try {
            String curVersion = HomePage.this.getPackageManager().getPackageInfo(package_name, 0).versionName;

            System.out.println("currentversion-----------" + curVersion);

            String newVersion = curVersion;

            newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + package_name + "&hl=en")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div[itemprop=softwareVersion]")
                    .first()
                    .ownText();

            System.out.println("Newversion-----------" + newVersion);

            return (value(curVersion) < value(newVersion)) ? true : false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    private void Alert(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_dialog_library, null);
        TextView alert_title = (TextView) dialogView.findViewById(R.id.custom_dialog_library_title_textview);
        TextView alert_message = (TextView) dialogView.findViewById(R.id.custom_dialog_library_message_textview);
        Button Bt_action = (Button) dialogView.findViewById(R.id.custom_dialog_library_ok_button);
        Bt_action.setText("Update now");
        Button Bt_dismiss = (Button) dialogView.findViewById(R.id.custom_dialog_library_cancel_button);
        Bt_dismiss.setVisibility(View.GONE);
        builder.setView(dialogView);
        alert_title.setText(title);
        alert_message.setText(message);
        builder.setCancelable(false);
        Bt_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + package_name)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + package_name)));
                }
            }
        });

        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {

            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP)
                    finish();
                return false;
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(HomePage.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        finish();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }



    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (session.isLoggedIn()) {
                        ChatingService.startDriverAction(HomePage.this);
                        Intent i = new Intent(getApplicationContext(), NavigationDrawer.class);
                        startActivity(i);
                        finish();
                    }

                } else {
                    finish();
                }
                break;
        }
    }
    private void requestCameraAndExternalPermission() {
        ActivityCompat.requestPermissions(this, PERMISSIONS_SELFIE, REQUEST_SELFIE);
    }

    /**
     * Enables https connections
     */
    @SuppressLint("TrulyRandom")
    public static void handleSSLHandshake() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
          /*  HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    Log.e("arg1",arg0+""+arg1);
                    return true;
                }
            });*/
        } catch (Exception ignored) {
        }
    }
}
