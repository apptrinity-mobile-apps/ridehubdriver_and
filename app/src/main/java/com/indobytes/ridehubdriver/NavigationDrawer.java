package com.indobytes.ridehubdriver;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.cardscanner.CardScanner;
import com.app.service.ServiceConstant;
import com.app.service.ServiceManager;
import com.indobytes.ridehubdriver.Utils.SessionManager;
import com.indobytes.ridehubdriver.widgets.CustomTextView;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import org.jsoup.Jsoup;

import java.util.HashMap;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

/**
 */
public class NavigationDrawer extends BaseActivity implements View.OnClickListener {

    private ResideMenu resideMenu;
    private ResideMenuItem itemHome;
    private ResideMenuItem itemReferal;
    private ResideMenuItem itemInviteandEarn;
    private ResideMenuItem itemTripsummary;
    private ResideMenuItem itemBankaccount;
    private ResideMenuItem itemPaymentStatement;
    private ResideMenuItem itemChangepassword;
    private ResideMenuItem itemLogout;
    private ResideMenuItem itemScanCard;
    private SessionManager session;
    private ActionBar actionBar;
    private String package_name = "com.indobytes.eweelsdriver";
    Dialog dialog;

    /**
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setIcon(R.drawable.drawer_icon);
        session = new SessionManager(NavigationDrawer.this);
        setUpMenu();
        web_update();
        Log.d("update", "stutd===" + web_update());
        if (web_update()) {
            Alert_update("Update Available", "Do you want to update.?");
        }
        if (savedInstanceState == null)
            changeFragment(new DashBoardDriver());

    }

    //--------------------------code to update checker------------------
    private long value(String string) {
        string = string.trim();
        if (string.contains(".")) {
            final int index = string.lastIndexOf(".");
            return value(string.substring(0, index)) * 100 + value(string.substring(index + 1));
        } else {
            return Long.valueOf(string);
        }
    }

    private boolean web_update() {
        try {
            String curVersion = getPackageManager().getPackageInfo(package_name, 0).versionName;

            System.out.println("currentversion-----------" + curVersion);

            String newVersion = curVersion;

            newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + package_name + "&hl=en")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div[itemprop=softwareVersion]")
                    .first()
                    .ownText();

            System.out.println("Newversion-----------" + newVersion);

            return (value(curVersion) < value(newVersion)) ? true : false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public ActionBar getActionBarSupport() {
        return actionBar;
    }

    private void setUpMenu() {
        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.reside_menu_bg_1);
        resideMenu.attachToActivity(this);
        resideMenu.setScaleValue(0.6f);
        resideMenu.setSwipeDirectionDisable(1);

        //resideMenu.setUse3D(true);


        // create menu items;
        String trip_summary = getResources().getString(R.string.trip_summary);
        itemHome = new ResideMenuItem(this, R.drawable.nav_home, "Home");
        itemTripsummary = new ResideMenuItem(this, R.mipmap.nav_tripsummary_icon, "" + trip_summary);
        itemBankaccount = new ResideMenuItem(this, R.mipmap.nav_bankaccount_icon, "Bank Account");
        itemPaymentStatement = new ResideMenuItem(this, R.mipmap.nav_paymentdetails_icon, "Payment Details");
        itemReferal = new ResideMenuItem(this, R.mipmap.nav_paymentdetails_icon, "Referral Points");
        itemInviteandEarn = new ResideMenuItem(this, R.mipmap.nav_paymentdetails_icon, "Invite and Earn");
        itemChangepassword = new ResideMenuItem(this, R.mipmap.nav_changepassword_icon, "Change Password");
        itemLogout = new ResideMenuItem(this, R.mipmap.nav_logout_icon, "Logout");
        //itemScanCard= new ResideMenuItem(this, R.drawable.icon_settings, "Scan Card");
        itemHome.setOnClickListener(this);
        itemTripsummary.setOnClickListener(this);
        itemBankaccount.setOnClickListener(this);
        itemPaymentStatement.setOnClickListener(this);
        itemReferal.setOnClickListener(this);
        itemInviteandEarn.setOnClickListener(this);
        itemChangepassword.setOnClickListener(this);
        itemLogout.setOnClickListener(this);
        //itemScanCard.setOnClickListener(this);


        resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemTripsummary, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemBankaccount, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemPaymentStatement, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemReferal, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemInviteandEarn, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemChangepassword, ResideMenu.DIRECTION_LEFT);
        //resideMenu.addMenuItem(itemScanCard,ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemLogout, ResideMenu.DIRECTION_LEFT);
    }

    private void Alert_update(String title, String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_dialog_library, null);
        TextView alert_title = (TextView) dialogView.findViewById(R.id.custom_dialog_library_title_textview);
        TextView alert_message = (TextView) dialogView.findViewById(R.id.custom_dialog_library_message_textview);
        Button Bt_action = (Button) dialogView.findViewById(R.id.custom_dialog_library_ok_button);
        Bt_action.setText("Update now");
        Button Bt_dismiss = (Button) dialogView.findViewById(R.id.custom_dialog_library_cancel_button);
        Bt_dismiss.setVisibility(View.GONE);
        builder.setView(dialogView);
        alert_title.setText(title);
        alert_message.setText(message);
        builder.setCancelable(false);
        Bt_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + package_name)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + package_name)));
                }
            }
        });

        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {

            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP)
                    finish();
                return false;
            }
        });

        dialog = builder.create();
        dialog.show();


    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        showBackPressedDialog(false);
    }

    private void showBackPressedDialog(final boolean isLogout) {
        System.gc();

        View dialogView;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            dialogView = inflater.inflate(R.layout.logout_alert, null);
        } else {
            dialogView = inflater.inflate(R.layout.logout_alert, null);
        }

        dialogBuilder.setView(dialogView);

        CustomTextView rl_yes = (CustomTextView) dialogView.findViewById(R.id.layout_logout_yes);

        CustomTextView rl_no = (CustomTextView) dialogView.findViewById(R.id.layout_logout_no);

        final AlertDialog b = dialogBuilder.create();
        b.show();
        rl_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Log.e("LOGOUT", "" + isLogout);
                if (!isLogout) {
                    Log.e("LOGOUTffff", "" + isLogout);
                    logout();
                }
                finish();
            }
        });
        rl_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });



            /*final PkDialog mDialog = new PkDialog(NavigationDrawer.this);
            mDialog.setDialogTitle(getResources().getString(R.string.profile_lable_logout_title));
            mDialog.setDialogMessage(getResources().getString(R.string.profile_lable_logout_message));
            mDialog.setPositiveButton(getResources().getString(R.string.profile_lable_logout_yes), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    if(isLogout){
                        logout();
                    }
                    finish();
                }
            });
            mDialog.setNegativeButton(getResources().getString(R.string.profile_lable_logout_no), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();*/


       /* AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_app_exiting)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(isLogout){
                            logout();
                        }
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        Dialog dialog = builder.create();
        dialog.show();*/
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {
        if (R.id.ic_menu == view.getId()) {
            resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
        } else if (view == itemHome) {
            changeFragment(new DashBoardDriver());
        } else if (view == itemTripsummary) {
            changeFragment(new TripSummeryList());
        } else if (view == itemBankaccount) {
            changeFragment(new BankDetails());
        } else if (view == itemPaymentStatement) {
            changeFragment(new PaymentDetails());
        } else if (view == itemReferal) {
            changeFragment(new ReferralPointsActivity());
        } else if (view == itemInviteandEarn) {
            changeFragment(new InviteAndEarn());
        } else if (view == itemChangepassword) {
            changeFragment(new ChangePassWord());
        } else if (view == itemLogout) {
            showBackPressedDialog(false);
        } else if (view == itemScanCard) {
            CardScanner cardScanner = new CardScanner(this);
            cardScanner.startScanActivityResult();
        }
        resideMenu.closeMenu();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CardScanner.MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr = "";
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";
                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                }
                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                }
                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }
            } else {
                resultDisplayStr = "Scan was canceled.";
            }
        }
    }

    private void logout() {

        // session.logoutUser();
        //showDialog("Logout");
        HashMap<String, String> jsonParams = new HashMap<String, String>();


        HashMap<String, String> userDetails = session.getUserDetails();
        String driverId = userDetails.get(SessionManager.KEY_DRIVERID);
        String login_time = userDetails.get(SessionManager.KEY_LOGINTIME);
        String login_id = userDetails.get(SessionManager.KEY_LOGINID);
        Log.e("LOGOUT", driverId + "--" + login_time + "--" + login_id);
        jsonParams.put("driver_id", "" + driverId);
        jsonParams.put("device", "" + "ANDROID");
        jsonParams.put("login_time", "" + login_time);
        jsonParams.put("login_id", "" + login_id);
        ServiceManager manager = new ServiceManager(this, updateAvailablityServiceListener);
        manager.makeServiceRequest(ServiceConstant.LOGOUT_REQUEST, Request.Method.POST, jsonParams);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                session.logoutUser();
            }
        });

        thread.start();
    }

    private ServiceManager.ServiceListener updateAvailablityServiceListener = new ServiceManager.ServiceListener() {
        @Override
        public void onCompleteListener(Object object) {
            dismissDialog();
        }

        @Override
        public void onErrorListener(Object error) {
            dismissDialog();
        }
    };

    private void changeFragment(Fragment targetFragment) {
        resideMenu.clearIgnoredViewList();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    // What good method is to access resideMenu？
    public ResideMenu getResideMenu() {
        return resideMenu;
    }
}


