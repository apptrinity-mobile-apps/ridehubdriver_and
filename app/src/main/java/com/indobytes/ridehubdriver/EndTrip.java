package com.indobytes.ridehubdriver;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.latlnginterpolation.LatLngInterpolator;
import com.app.service.SensorService;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.app.xmpp.ChatingService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.vision.barcode.Barcode;
import com.indobytes.ridehubdriver.Utils.ConnectionDetector;
import com.indobytes.ridehubdriver.Utils.CurrencySymbolConverter;
import com.indobytes.ridehubdriver.Utils.GPSTracker;
import com.indobytes.ridehubdriver.Utils.SessionManager;
import com.indobytes.ridehubdriver.adapter.ContinuousRequestAdapter;
import com.indobytes.ridehubdriver.googlemappath.GMapV2GetRouteDirection;
import com.indobytes.ridehubdriver.subclass.RealTimeActivity;
import com.indobytes.ridehubdriver.widgets.PkDialog;

import org.jivesoftware.smack.chat.Chat;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by user88 on 10/29/2015.
 */
public class EndTrip extends RealTimeActivity implements com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, SeekBar.OnSeekBarChangeListener {
    private final static int REQUEST_LOCATION = 199;
    private PendingResult<LocationSettingsResult> result;
    private String driver_id = "",category_type = "";
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private TextView Tv_name, Tv_mobilno, Tv_rideid, Tv_start_wait, Tv_stop_wait;
    private RelativeLayout Rl_layout_back;
    private Button Bt_Endtrip;
    private String Str_name = "", Str_mobilno = "", Str_rideid = "",Str_User_Id="";
    String Str_otp="",Str_amount="",totalDistanceTravel="";
    private RelativeLayout alert_layout;
    private TextView alert_textview;
    private String droplocation[];
    private String startlocation[];
    private MarkerOptions marker;
    private double previous_lat, previous_lon, current_lat, current_lon, dis = 0.0;
    public static Location myLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Marker currentMarker;
    private static Marker movingMarker;
    private ServiceRequest mRequest;
    private GoogleMap googleMap;
    private Dialog dialog;
    private GMapV2GetRouteDirection v2GetRouteDirection;
    private Document document;
    private int mins;
    private int secs;
    private int hours;

    private int milliseconds;
    private Button Bt_Enable_voice;
    private String Str_status = "", Str_response = "", Str_ridefare = "", Str_timetaken = "", Str_waitingtime = "", Str_need_payment = "", Str_currency = "", Str_ride_distance = "", str_recievecash = "";
    private GPSTracker gps;
    private LatLng destlatlng, startlatlng;
    private double MyCurrent_lat = 0.0, MyCurrent_long = 0.0;
    private TextView timerValue;
    private RelativeLayout layout_timer;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    private long timeInMilliseconds = 0L;
    private long timeSwapBuff = 0L;
    private long updatedTime = 0L;
    float[] results;
    LocationManager locationManager;
    Barcode.GeoPoint geoPoint;
    double location;
    private String beginAddress;
    private String endAddress;
    private String sCurrencySymbol = "";
    private String distance = "";
    private LatLng latLng;
    private PolylineOptions mPolylineOptions;
    private SeekBar sliderSeekBar;
    //private ShimmerButton Bt_slider; chaitanya
    private Button Bt_slider;
    private float distance_to = 0;
    double startlatitude = 0.0,startlongitude = 0.0;
    private float v;
    private LatLng startPositionn;
    private String Str_Latitude = "", Str_longitude = "";

    private final static int INTERVAL = 10000;
    Handler mHandler;

    private MarkerOptions markerOptions;
    private RelativeLayout Rl_layout_enable_voicenavigation;

    Intent mServiceIntent;
    private SensorService mSensorService;

    private boolean isFirstPosition = true;
    private LatLng startPosition;
    private LatLng endPosition;
    private double lat, lng;
    private Boolean firsttimepolyline = true;


    private Polyline polyline;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.endtrip);


        initialize();
        mSensorService = new SensorService(getApplicationContext());
        mServiceIntent = new Intent(getApplicationContext(), mSensorService.getClass());

        try {
            setLocationRequest();
            buildGoogleApiClient();
            initializeMap();
        } catch (Exception e) {
        }

        ChatingService.startDriverAction(EndTrip.this);

        Tv_start_wait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tv_stop_wait.setVisibility(View.VISIBLE);
                Tv_start_wait.setVisibility(View.GONE);
                layout_timer.setVisibility(View.VISIBLE);
                startTime = SystemClock.uptimeMillis();
                customHandler.postDelayed(updateTimerThread, 0);
            }
        });
        Tv_stop_wait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tv_start_wait.setVisibility(View.VISIBLE);
                Tv_stop_wait.setVisibility(View.GONE);

                timeSwapBuff += timeInMilliseconds;
                customHandler.removeCallbacks(updateTimerThread);

            }
        });
    }


    private void initialize() {
        session = new SessionManager(EndTrip.this);
        gps = new GPSTracker(EndTrip.this);
        mHandler = new Handler();
        markerOptions = new MarkerOptions();



        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
        //category_type = user.get(SessionManager.KEY_CATEGORY_TYPE);


        v2GetRouteDirection = new GMapV2GetRouteDirection();
        Bundle b = getIntent().getExtras();

        if (b != null && b.containsKey("pickuplatlng")) {
            droplocation = b.getString("pickuplatlng").split(",");
        }
        if (b != null && b.containsKey("startpoint")) {
            beginAddress = b.getString("startpoint");
            startlocation = b.getString("startpoint").split(",");
            try {
                double latitude = Double.parseDouble(droplocation[0]);
                double longitude = Double.parseDouble(droplocation[1]);
                startlatitude = Double.parseDouble(startlocation[0]);
                startlongitude = Double.parseDouble(startlocation[1]);
                destlatlng = new LatLng(latitude, longitude);
                startlatlng = new LatLng(startlatitude, startlongitude);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //---------------set polyline color and width----------------
        mPolylineOptions = new PolylineOptions();
        mPolylineOptions.color(Color.BLUE).width(10);
        Intent i = getIntent();
        Str_rideid = i.getStringExtra("rideid");
        Str_name = i.getStringExtra("name");
        Str_mobilno = i.getStringExtra("mobilno");
        Str_User_Id = i.getStringExtra("user_id");

        ContinuousRequestAdapter.userID = Str_User_Id;

        Tv_name = (TextView) findViewById(R.id.end_trip_name);
        Tv_mobilno = (TextView) findViewById(R.id.end_trip_mobilno);
        Tv_rideid = (TextView) findViewById(R.id.beginendtrip_rideid);
        Tv_start_wait = (TextView) findViewById(R.id.begin_waitingtime_tv_start);
        Tv_stop_wait = (TextView) findViewById(R.id.begin_waitingtime_tv_stop);
        /*if(category_type.equalsIgnoreCase("Jcb")){
            Tv_start_wait.setVisibility(View.GONE);
            Tv_stop_wait.setVisibility(View.GONE);
        }*/
        timerValue = (TextView) findViewById(R.id.timerValue);
        layout_timer = (RelativeLayout) findViewById(R.id.layout_timer);
        alert_layout = (RelativeLayout) findViewById(R.id.end_trip_alert_layout);
        alert_textview = (TextView) findViewById(R.id.end_trip_alert_textView);
        Rl_layout_enable_voicenavigation = (RelativeLayout) findViewById(R.id.layout_arrived_Enable_voice);


        Rl_layout_enable_voicenavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String voice_curent_lat_long = MyCurrent_lat + "," + MyCurrent_long;
                String voice_destination_lat_long = droplocation[0] + "," + droplocation[1];
                System.out.println("----------fromPosition---------------" + voice_curent_lat_long);
                System.out.println("----------toPosition---------------" + voice_destination_lat_long);
                String locationUrl = "http://maps.google.com/maps?saddr=" + voice_curent_lat_long + "&daddr=" + voice_destination_lat_long;
                System.out.println("----------locationUrl---------------" + locationUrl);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(locationUrl));
                startActivity(intent);

                if (!isMyServiceRunning(mSensorService.getClass())) {
                    mServiceIntent.putExtra("rideid",Str_rideid);
                    mServiceIntent.putExtra("driverid",driver_id);
                    mServiceIntent.putExtra("chatID",chatID);
                    Log.d("send service",Str_rideid+"-"+driver_id);
                    startService(mServiceIntent);
                }

            }
        });


      //  shimmer = new Shimmer();
        //sliderSeekBar = (SeekBar) findViewById(R.id.end_Trip_seek);
        //Bt_slider = (ShimmerButton) findViewById(R.id.end_Trip_slider_button);
        //shimmer.start(Bt_slider);  chaitanya
        Bt_slider = (Button) findViewById(R.id.end_Trip_slider_button);
        Bt_slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("------------------sliding completed----------------");
                mSensorService.stoptimertask();
                cd = new ConnectionDetector(EndTrip.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    PostRequest(ServiceConstant.endtrip_url);
                    System.out.println("end------------------" + ServiceConstant.endtrip_url);
                } else {

                    Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                }
            }
        });
        //sliderSeekBar.setOnSeekBarChangeListener(this); chaitanya

        timerValue.setText("00:00:00");


        Tv_name.setText(Str_name);
        Tv_mobilno.setText(Str_mobilno);

        cd = new ConnectionDetector(EndTrip.this);
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            mHandlerTask.run();
        } else {
            Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
        }
    }


    private void initializeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) EndTrip.this.getFragmentManager().findFragmentById(R.id.arrived_trip_view_map)).getMap();
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(EndTrip.this, getResources().getString(R.string.action_alert_unabletocreatemap), Toast.LENGTH_SHORT).show();
            }
        }
        // Changing map type
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // Showing / hiding your current location
        googleMap.setMyLocationEnabled(false);
        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(false);
        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.setMyLocationEnabled(false);


        if (gps.canGetLocation()) {
            double Dlatitude = gps.getLatitude();
            double Dlongitude = gps.getLongitude();

            MyCurrent_lat = Dlatitude;
            MyCurrent_long = Dlongitude;

            previous_lat = MyCurrent_lat;
            previous_lon = MyCurrent_long;
            // Move the camera to last position with a zoom level
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            //----------------------set marker------------------
            marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
            marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.car_icon));
            currentMarker = googleMap.addMarker(marker);

        } else {
            alert_layout.setVisibility(View.VISIBLE);
            alert_textview.setText(getResources().getString(R.string.alert_gpsEnable));
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(EndTrip.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                Intent broadcastIntent_begintrip = new Intent();
                broadcastIntent_begintrip.setAction("com.finish.com.finish.BeginTrip");
                sendBroadcast(broadcastIntent_begintrip);

                Intent broadcastIntent_arrivedtrip = new Intent();
                broadcastIntent_arrivedtrip.setAction("com.finish.ArrivedTrip");
                sendBroadcast(broadcastIntent_arrivedtrip);

                Intent broadcastIntent_endtrip = new Intent();
                broadcastIntent_endtrip.setAction("com.finish.EndTrip");
                sendBroadcast(broadcastIntent_endtrip);
                Intent intent = new Intent(EndTrip.this, LoadingPage.class);
                intent.putExtra("Driverid", driver_id);
                intent.putExtra("RideId", Str_rideid);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
        mDialog.show();
    }


    //------------------------------code for distance----------------------------
    @Override
    protected void onResume() {
        super.onResume();
        mHandlerTask.run();
        Log.e("ENDTRIP_ONRESUME","ONRESUME" );
        startLocationUpdates();
        //stopService(mServiceIntent);
        // Log.i("MAINACT","onDestroy!");

    }

    private void setLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(2000);
        mLocationRequest.setFastestInterval(2000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        mHandler.removeCallbacks(mHandlerTask);
    }


    public void onConnected(Bundle bundle) {

        if (gps != null && gps.canGetLocation() && gps.isgpsenabled()) {
        }
        myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);

        if (myLocation != null) {
            if (googleMap == null)
                googleMap = ((MapFragment) EndTrip.this.getFragmentManager().findFragmentById(R.id.arrived_trip_view_map)).getMap();
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 16));

            if (startlatlng != null && destlatlng != null) {
                GetRouteTask getRoute = new GetRouteTask();
                getRoute.execute();
            }
        }
    }

    public void onConnectionSuspended(int i) {
    }

    JSONObject job;
    @Override
    public void onLocationChanged(Location location) {

        if(this.myLocation != null)
        {
            distance_to = location.distanceTo(myLocation);
            System.out.println("---------distance to-----------"+location.distanceTo(myLocation));
        }
        this.myLocation = location;
        if (myLocation != null) {

            //-------------Updating Marker-------
            try {

                MyCurrent_lat = myLocation.getLatitude();
                MyCurrent_long = myLocation.getLongitude();

                LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                if (currentMarker != null) {
                    currentMarker.remove();
                }
                currentMarker = googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.car_icon)));

                //updateDriverOnMap(myLocation.getLatitude(),myLocation.getLongitude());

                float zoom = googleMap.getCameraPosition().zoom;
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
                CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                googleMap.moveCamera(camUpdate);

                sendLocationToUser(myLocation);
                updateDriverOnMap(MyCurrent_lat,MyCurrent_long);
            } catch (Exception e) {
            }


            //------Calculating Distance------
            float[] f = new float[1];
            current_lat = location.getLatitude();
            current_lon = location.getLongitude();
            if (current_lat != previous_lat || current_lon != previous_lon) {

                if( distance_to >= 1.0) {
                    Location.distanceBetween(previous_lat, previous_lon, current_lat, current_lon, f);
                    dis += Double.parseDouble(String.valueOf(f[0]));
                }
                previous_lat = current_lat;
                previous_lon = current_lon;
                System.out.println("distance inside----------------------" + dis);
            } else {
                previous_lat = current_lat;
                previous_lon = current_lon;
                dis = dis;
            }
            previous_lat = current_lat;
            previous_lon = current_lon;

        }
    }


    Chat chat;

    private void sendLocationToUser(Location location) throws JSONException {



        String sendLat = Double.valueOf(location.getLatitude()).toString();
        String sendLng = Double.valueOf(location.getLongitude()).toString();

        System.out.println("endtripchatID--------------"+chatID+"--"+sendLat+"--"+sendLat+"--"+Str_rideid);
        if (job == null) {
            job = new JSONObject();
        }
        job.put("action", "driver_loc");
        job.put("latitude", sendLat);
        job.put("longitude", sendLng);
        job.put("device_type","android");
        job.put("background","no");
        job.put("ride_id", Str_rideid);
        builder.sendMessage(chatID, job.toString());


       /* String sToID = ContinuousRequestAdapter.userID + "@" + ServiceConstant.XMPP_SERVICE_NAME;
        try {
            if(chat  != null){
                chat.sendMessage(job.toString());
            }else{
                chat = ChatingService.createChat(sToID);
                chat.sendMessage(job.toString());
            }
        } catch (SmackException.NotConnectedException e) {
            try {
                chat = ChatingService.createChat(sToID);
                chat.sendMessage(job.toString());
            }catch (SmackException.NotConnectedException e1){
                Toast.makeText(this,"Not Able to send data to the user Network Error",Toast.LENGTH_SHORT).show();
            }
        }*/

    }


    private class GetRouteTask extends AsyncTask<String, Void, String> {

        String response = "";
        GMapV2GetRouteDirection v2GetRouteDirection = new GMapV2GetRouteDirection();
        Document document;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... urls) {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(startlatlng, destlatlng, GMapV2GetRouteDirection.MODE_DRIVING);
            response = "Success";
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            if(polyline!= null){
                polyline.remove();
            }
            if (result.equalsIgnoreCase("Success")) {
                ArrayList<LatLng> directionPoint = v2GetRouteDirection.getDirection(document);
                PolylineOptions rectLine = new PolylineOptions().width(10).color(getResources().getColor(R.color.app_color));
                for (int i = 0; i < directionPoint.size(); i++) {
                    rectLine.add(directionPoint.get(i));
                }



                if(firsttimepolyline){

                    Marker m[] = new Marker[2];
                    m[0] = googleMap.addMarker(new MarkerOptions().position(startlatlng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.current_trackride)));
                    m[1] = googleMap.addMarker(new MarkerOptions().position(destlatlng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.desti_trackride)));
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (Marker marker : m) {
                        builder.include(marker.getPosition());
                    }
                    LatLngBounds bounds = builder.build();
                    int padding = 100; // offset from edges of the map in pixels
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

                    googleMap.moveCamera(cu);
                    googleMap.animateCamera(cu);

                    markerOptions.position(startlatlng);
                }

                markerOptions.position(destlatlng);



                // Adding route on the map
                polyline =  googleMap.addPolyline(rectLine);

                markerOptions.draggable(true);
            }
        }
    }


    public double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double latA = Math.toRadians(lat1);
        double lonA = Math.toRadians(lon1);
        double latB = Math.toRadians(lat2);
        double lonB = Math.toRadians(lon2);
        double cosAng = (Math.cos(latA) * Math.cos(latB) * Math.cos(lonB - lonA)) +
                (Math.sin(latA) * Math.sin(latB));
        double ang = Math.acos(cosAng);
        double dist = ang * 6371;
        return dist;
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;
/*
            secs = (int) (updatedTime / 1000);
            mins = secs / 60;
            secs = secs % 60;
            milliseconds = (int) (updatedTime % 1000);
            timerValue.setText("" + mins + ":"
                    + String.format("%02d", secs));
            */

            secs = (int) (updatedTime / 1000);
            hours = secs / (60 * 60);
            mins = secs / 60;
            secs = secs % 60;
            if (mins >= 60) {
                mins = 0;
            }
            milliseconds = (int) (updatedTime % 1000);
            timerValue.setText(hours + ":" + mins + ":"
                    + String.format("%02d", secs));

            System.out.println("thread----------------"+timerValue);

            customHandler.postDelayed(this, 0);


        }

    };


    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {

            gps = new GPSTracker(EndTrip.this);
            cd = new ConnectionDetector(EndTrip.this);
            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                if (gps != null && gps.canGetLocation() && gps.isgpsenabled()) {

                    Str_Latitude = String.valueOf(gps.getLatitude());
                    Str_longitude = String.valueOf(gps.getLongitude());

                    postRequest_UpdateProviderLocation(ServiceConstant.UPDATE_CURRENT_LOCATION);

                }
            } else {
                Toast.makeText(EndTrip.this, getResources().getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }

            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };


    public static final float[] calculateDistanceTo(Location fromLocation, Location toLocation) {
        float[] results = new float[0];
        double startLatitude = fromLocation.getLatitude();
        double startLongitude = fromLocation.getLongitude();
        double endLatitude = toLocation.getLatitude();
        double endLongitude = toLocation.getLongitude();
        fromLocation.distanceBetween(startLatitude, startLongitude, endLatitude, endLongitude, results);
        return results;
    }


    /*
    public double getDistance(double a[]) {
        double earthRadius = 6371; //kilometers
        double dLat = Math.toRadians(a[2] -a[0] );
        double dLng = Math.toRadians(a[3] - a[1]);
        double b = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(a[0])) * Math.cos(Math.toRadians(a[2])) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(b), Math.sqrt(1-b));
        float dist = (float) (earthRadius * c);

        return dist;
    }
*/


    //-------------------Show Summery fare  Method--------------------
    private void showfaresummerydetails() {

        final MaterialDialog dialog = new MaterialDialog(EndTrip.this);
        View view = LayoutInflater.from(EndTrip.this).inflate(R.layout.fare_summery_alert_dialog, null);
        final TextView Tv_reqest = (TextView) view.findViewById(R.id.requst);
        TextView tv_fare_totalamount = (TextView) view.findViewById(R.id.fare_summery_total_amount);
        TextView tv_ridedistance = (TextView) view.findViewById(R.id.fare_summery_ride_distance_value);
        TextView tv_timetaken = (TextView) view.findViewById(R.id.fare_summery_ride_timetaken_value);
        TextView tv_waittime = (TextView) view.findViewById(R.id.fare_summery_wait_time_value);
        RelativeLayout layout_request_payment = (RelativeLayout) view.findViewById(R.id.layout_faresummery_requstpayment);
        RelativeLayout layout_receive_cash = (RelativeLayout) view.findViewById(R.id.fare_summery_receive_cash_layout);
        tv_fare_totalamount.setText(Str_ridefare);
        tv_ridedistance.setText(Str_ride_distance);
        tv_timetaken.setText(Str_timetaken);
        tv_waittime.setText(Str_waitingtime);
        dialog.setView(view).show();
        //if (Str_need_payment.equalsIgnoreCase("YES")){
        layout_receive_cash.setVisibility(View.VISIBLE);
        layout_request_payment.setVisibility(View.VISIBLE);
        Tv_reqest.setText(EndTrip.this.getResources().getString(R.string.lbel_fare_summery_requestpayment));


        layout_receive_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /*Intent intent = new Intent(EndTrip.this, OtpPage.class); chaitanya
                intent.putExtra("rideid", Str_rideid);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
                ////chaitanya////
                cd = new ConnectionDetector(EndTrip.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent){
                    PostRequest_payment(ServiceConstant.receivecash_url);
                    System.out.println("end------------------" + ServiceConstant.receivecash_url);
                }else {

                    Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                }
                ////chaitanya///
            }
        });

        layout_request_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(EndTrip.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {

                    if (Tv_reqest.getText().toString().equalsIgnoreCase(EndTrip.this.getResources().getString(R.string.lbel_fare_summery_requestpayment))) {
                        postRequest_Reqqustpayment(ServiceConstant.request_paymnet_url);
                        System.out.println("arrived------------------" + ServiceConstant.request_paymnet_url);
                    } else {
                        Intent intent = new Intent(EndTrip.this, RatingsPage.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }

                } else {
                    Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                }
            }
        });

    }

    private void showfaresummerydetails1() {

        final MaterialDialog dialog = new MaterialDialog(EndTrip.this);
        View view = LayoutInflater.from(EndTrip.this).inflate(R.layout.fare_summery_alert_dialog, null);
        final TextView Tv_reqest = (TextView) view.findViewById(R.id.requst);
        TextView tv_fare_totalamount = (TextView) view.findViewById(R.id.fare_summery_total_amount);
        TextView tv_ridedistance = (TextView) view.findViewById(R.id.fare_summery_ride_distance_value);
        TextView tv_timetaken = (TextView) view.findViewById(R.id.fare_summery_ride_timetaken_value);
        TextView tv_waittime = (TextView) view.findViewById(R.id.fare_summery_wait_time_value);
        RelativeLayout layout_request_payment = (RelativeLayout) view.findViewById(R.id.layout_faresummery_requstpayment);
        RelativeLayout layout_receive_cash = (RelativeLayout) view.findViewById(R.id.fare_summery_receive_cash_layout);
        tv_fare_totalamount.setText(Str_ridefare);
        tv_ridedistance.setText(Str_ride_distance);
        tv_timetaken.setText(Str_timetaken);
        tv_waittime.setText(Str_waitingtime);
        dialog.setView(view).show();


        // if (Str_need_payment.equalsIgnoreCase("YES")){

        layout_receive_cash.setVisibility(View.GONE);
        layout_request_payment.setVisibility(View.VISIBLE);
        Tv_reqest.setText(EndTrip.this.getResources().getString(R.string.lbel_fare_summery_requestpayment));

        //  }else{
        // layout_receive_cash.setVisibility(View.GONE);
        //  Tv_reqest.setText(EndTrip.this.getResources().getString(R.string.alert_label_ok));

        // }
//
        /*layout_receive_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EndTrip.this, OtpPage.class);
                intent.putExtra("rideid", Str_rideid);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });*/

        layout_request_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(EndTrip.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {

                    if (Tv_reqest.getText().toString().equalsIgnoreCase(EndTrip.this.getResources().getString(R.string.lbel_fare_summery_requestpayment))) {
                        postRequest_Reqqustpayment(ServiceConstant.request_paymnet_url);
                        System.out.println("arrived------------------" + ServiceConstant.request_paymnet_url);
                    } else {
                        Intent intent = new Intent(EndTrip.this, RatingsPage.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }

                } else {
                    Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                }
            }
        });

    }


    private void showfaresummerydetails2() {

        final MaterialDialog dialog = new MaterialDialog(EndTrip.this);
        View view = LayoutInflater.from(EndTrip.this).inflate(R.layout.fare_summery_alert_dialog, null);
        final TextView Tv_reqest = (TextView) view.findViewById(R.id.requst);
        TextView tv_fare_totalamount = (TextView) view.findViewById(R.id.fare_summery_total_amount);
        TextView tv_ridedistance = (TextView) view.findViewById(R.id.fare_summery_ride_distance_value);
        TextView tv_timetaken = (TextView) view.findViewById(R.id.fare_summery_ride_timetaken_value);
        TextView tv_waittime = (TextView) view.findViewById(R.id.fare_summery_wait_time_value);
        RelativeLayout layout_request_payment = (RelativeLayout) view.findViewById(R.id.layout_faresummery_requstpayment);
        RelativeLayout layout_receive_cash = (RelativeLayout) view.findViewById(R.id.fare_summery_receive_cash_layout);
        tv_fare_totalamount.setText(Str_ridefare);
        tv_ridedistance.setText(Str_ride_distance);
        tv_timetaken.setText(Str_timetaken);
        tv_waittime.setText(Str_waitingtime);
        dialog.setView(view).show();


        // if (Str_need_payment.equalsIgnoreCase("YES")){

        layout_receive_cash.setVisibility(View.GONE);
        layout_request_payment.setVisibility(View.VISIBLE);
        Tv_reqest.setText(EndTrip.this.getResources().getString(R.string.lbel_notification_ok));

        //  }else{
        // layout_receive_cash.setVisibility(View.GONE);
        //  Tv_reqest.setText(EndTrip.this.getResources().getString(R.string.alert_label_ok));

        // }
//
        /*layout_receive_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EndTrip.this, OtpPage.class);
                intent.putExtra("rideid", Str_rideid);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });*/

        layout_request_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(EndTrip.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {


                    Intent intent = new Intent(EndTrip.this, RatingsPage.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


                } else {
                    Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                }
            }
        });

    }

    //-----------------------Code for begin trip post request-----------------
    private void PostRequest(String Url) {
        dialog = new Dialog(EndTrip.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------endtrip----------------" + Url);
        System.out.println("-------------wait_time----------------" + String.valueOf(mins));


          /* float[] f = new float[2];
        System.out.println(droplocation[0]+"=="+droplocation[1]+"-------------CURRENT LAT LNG----------------" + current_lat+"--"+current_lon);
            if (current_lat == 0.0 || current_lon == 0.0) {
                current_lat = Double.valueOf(droplocation[0]);
                current_lon = Double.valueOf(droplocation[1]);
            }
        System.out.println("-------------AFETR CURRENT LAT LNG----------------" + current_lat+"--"+current_lon);*/
        //Location.distanceBetween(startlatitude, startlongitude, current_lat, current_lon, f);
        //Double orijinal_dis= Double.parseDouble(String.valueOf(f[0]));

            /*if((dis / 1000) > (orijinal_dis / 1000)){
                dis = dis / 1000;
            }else{*/
        // dis = orijinal_dis / 1000;
        //}


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("ride_id", Str_rideid);
        jsonParams.put("drop_lat", String.valueOf(MyCurrent_lat));
        jsonParams.put("drop_lon", String.valueOf(MyCurrent_long));
        jsonParams.put("distance", String.valueOf(dis/1000));
        jsonParams.put("wait_time", String.valueOf(timerValue.getText().toString()));

        //jsonParams.put("wait_time",String.valueOf(mins).replace(":","."));

               /* jsonParams.put("wait_time",String.valueOf( String.valueOf("" + mins + ":"
                        + String.format("%02d", secs) + ":"
                        + String.format("%03d", milliseconds))).replace(":","."));*/
        System.out
                .println("--------------driver_id-------------------"
                        + driver_id);
        System.out
                .println("--------------drop_lat-------------------"
                        + String.valueOf(MyCurrent_lat));
        System.out
                .println("--------------drop_lon-------------------"
                        + String.valueOf(MyCurrent_long));

        System.out
                .println("--------------postdistance-------------------"
                        + String.valueOf(dis / 1000));



        System.out
                .println("--------------wait_time-------------------"
                        + String.valueOf(timerValue.getText().toString()));



        mRequest = new ServiceRequest(EndTrip.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                Log.e("end", response);

                System.out.println("endtrip---------" + response);

                //  String Str_status = "",Str_response="",Str_ridefare="",Str_timetaken="",Str_waitingtime="",Str_currency="",Str_ride_distance="";

                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    Str_response = object.getString("response");

                    JSONObject jsonObject = object.getJSONObject("response");
                    JSONObject jobject = jsonObject.getJSONObject("fare_details");
                    Str_need_payment = jsonObject.getString("need_payment");
                    str_recievecash = jsonObject.getString("receive_cash");
                    Str_currency = jobject.getString("currency");

                    //Currency currencycode = Currency.getInstance(getLocale(Str_currency));
                    sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(Str_currency);

                    Str_ridefare = sCurrencySymbol + jobject.getString("ride_fare");
                    Str_timetaken = jobject.getString("ride_duration");
                    Str_waitingtime = jobject.getString("waiting_duration");
                    Str_ride_distance = jobject.getString("ride_distance");
                    Str_need_payment = jobject.getString("need_payment");


                    Log.d("RECEIVE", str_recievecash);


                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();


                if (Str_status.equalsIgnoreCase("1")) {

                    //  endTripHandler.removeCallbacks(endTripRunnable);


                    if (Str_need_payment.equalsIgnoreCase("YES")) {
                        System.out.println("sucess------------" + Str_need_payment);
                        if (str_recievecash.matches("Enable")) {
                            showfaresummerydetails();
                        } else {
                            showfaresummerydetails1();
                        }

                    } else {
                        showfaresummerydetails2();
                    }

                } else {
                    Alert(getResources().getString(R.string.alert_sorry_label_title), Str_response);
                }


                dialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }

        });
    }
    //-----------------------Code for arrived post request-----------------chaitanya
    private void PostRequest_payment(String Url) {
        dialog = new Dialog(EndTrip.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------otp----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id",driver_id);
        jsonParams.put("ride_id",Str_rideid);

        System.out.println("otp-------driver_id---------"+driver_id);

        System.out.println("otp-------ride_id---------"+Str_rideid);
        mRequest = new ServiceRequest(EndTrip.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("otp", response);

                System.out.println("otp---------"+response);

                String Str_status = "",Str_response="",Str_otp_status="",Str_ride_id="",Str_currency="";

                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");

                    if (Str_status.equalsIgnoreCase("1")){

                        Str_response = object.getString("response");
                        Str_currency = object.getString("currency");
                        //Currency currencycode = Currency.getInstance(getLocale(Str_currency));

                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(Str_currency);

                        Str_otp_status = object.getString("otp_status");
                        Str_otp  = object.getString("otp");
                        Str_ride_id = object.getString("ride_id");
                        Str_amount = sCurrencySymbol + object.getString("amount");

                        System.out.println("otp--------"+Str_otp);

                        System.out.println("Str_otp_status--------"+Str_otp_status);
                        Intent intent = new Intent(EndTrip.this, PaymentPage.class);
                        intent.putExtra("amount", Str_amount);
                        intent.putExtra("rideid", Str_rideid);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }else{

                        Str_response = object.getString("response");
                    }
                }catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();

                /////chaitanya
               /* if (Str_status.equalsIgnoreCase("1")){

                    if (Str_otp_status.equalsIgnoreCase("development")){
                        Et_otp.setText(Str_otp);
                    }

                }else{
                    Alert(getResources().getString(R.string.alert_sorry_label_title), Str_response);  }*/
            }

            @Override
            public void onErrorListener() {

                dialog.dismiss();
            }
        });

    }
 /*           private void PostRequest1(String Url) {
        dialog = new Dialog(EndTrip.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title=(TextView)dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        postrequest = new StringRequest(Request.Method.POST, Url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("end", response);

                        System.out.println("endtrip---------"+response);

                      //  String Str_status = "",Str_response="",Str_ridefare="",Str_timetaken="",Str_waitingtime="",Str_currency="",Str_ride_distance="";

                       try {
                            JSONObject object = new JSONObject(response);
                            Str_status = object.getString("status");
                           Str_response = object.getString("response");

                           JSONObject jsonObject= object.getJSONObject("response");
                           JSONObject jobject = jsonObject.getJSONObject("fare_details");

                           Str_currency = jobject.getString("currency");

                           //Currency currencycode = Currency.getInstance(getLocale(Str_currency));
                           sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(Str_currency);

                           Str_ridefare = sCurrencySymbol + jobject.getString("ride_fare");
                           Str_timetaken = jobject.getString("ride_duration");
                           Str_waitingtime = jobject.getString("waiting_duration");
                           Str_ride_distance = jobject.getString("ride_distance");
                           Str_need_payment = jobject.getString("need_payment");

                        }catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                        if (Str_status.equalsIgnoreCase("1")){

                            endTripHandler.removeCallbacks(endTripRunnable);

                            if (Str_need_payment.equalsIgnoreCase("YES")){
                                System.out.println("sucess------------"+Str_need_payment);
                                showfaresummerydetails();
                            }else{
                                showfaresummerydetails();
                            }

                        }else {
                            Alert(getResources().getString(R.string.alert_sorry_label_title), Str_response);

                        }

                        dialog.dismiss();

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorResponse.VolleyError(EndTrip.this, error);
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("User-agent", ServiceConstant.useragent);
                headers.put("isapplication",ServiceConstant.isapplication);
                headers.put("applanguage",ServiceConstant.applanguage);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id",driver_id);
                jsonParams.put("ride_id",Str_rideid);
                jsonParams.put("drop_lat",String.valueOf(MyCurrent_lat));
                jsonParams.put("drop_lon",String.valueOf(MyCurrent_long));
                jsonParams.put("distance",String.valueOf(dis/1000));
                jsonParams.put("wait_time","0");

                //jsonParams.put("wait_time",String.valueOf(mins).replace(":","."));
               *//* jsonParams.put("wait_time",String.valueOf( String.valueOf("" + mins + ":"
                        + String.format("%02d", secs) + ":"
                        + String.format("%03d", milliseconds))).replace(":","."));*//*


                System.out
                        .println("--------------driver_id-------------------"
                                + driver_id);
                System.out
                        .println("--------------drop_lat-------------------"
                                + String.valueOf(MyCurrent_lat));
                System.out
                        .println("--------------drop_lon-------------------"
                                + String.valueOf(MyCurrent_long));

                System.out
                        .println("--------------postdistance-------------------"
                                +String.valueOf(dis/1000));




                return jsonParams;
            }
        };
         postrequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                 DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                 DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
         postrequest.setShouldCache(false);

        AppController.getInstance().addToRequestQueue(postrequest);
    }*/


    //-----------------------Code for arrived post request-----------------
    private void postRequest_Reqqustpayment(String Url) {
        dialog = new Dialog(EndTrip.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);

        dialog_title.setText(getResources().getString(R.string.action_loading));
     /*  LinearLayout main = (LinearLayout)findViewById(R.id.main_layout);
        View view = getLayoutInflater().inflate(R.layout.waiting, main,false);
        main.addView(view);
*/

        System.out.println("-------------endtrip----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("ride_id", Str_rideid);

        System.out
                .println("--------------driver_id-------------------"
                        + driver_id);


        System.out
                .println("--------------ride_id-------------------"
                        + Str_rideid);

        mRequest = new ServiceRequest(EndTrip.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                Log.e("requestpayment", response);

                System.out.println("response---------" + response);

                String Str_status = "", Str_response = "", Str_currency = "", Str_rideid = "", Str_action = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Str_response = object.getString("response");
                    Str_status = object.getString("status");

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("0")) {
                    Alert(getResources().getString(R.string.alert_sorry_label_title), Str_response);

                } else {
                    Alert(getResources().getString(R.string.label_pushnotification_cashreceived), Str_response);
                }
            }

            @Override
            public void onErrorListener() {

                dialog.dismiss();
            }

        });

    }


/*            private void postRequest_Reqqustpayment1(String Url) {
        dialog = new Dialog(EndTrip.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        System.out.println("loadin-----------");
        TextView dialog_title=(TextView)dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        postrequest = new StringRequest(Request.Method.POST, Url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("requestpayment", response);

                        System.out.println("response---------"+response);

                        String Str_status = "",Str_response="",Str_currency="",Str_rideid="",Str_action="";

                        try {
                            JSONObject object = new JSONObject(response);
                            Str_response = object.getString("response");
                            Str_status = object.getString("status");

                        }catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        if (Str_status.equalsIgnoreCase("0"))
                        {
                            Alert(getResources().getString(R.string.alert_sorry_label_title), Str_response);

                        }else{
                            Alert(getResources().getString(R.string.label_pushnotification_cashreceived), Str_response);

                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorResponse.VolleyError(EndTrip.this, error);
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("User-agent",ServiceConstant.useragent);
                headers.put("isapplication",ServiceConstant.isapplication);
                headers.put("applanguage",ServiceConstant.applanguage);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", driver_id);
                jsonParams.put("ride_id", Str_rideid);

                System.out
                        .println("--------------driver_id-------------------"
                                + driver_id);


                System.out
                        .println("--------------ride_id-------------------"
                                + Str_rideid);


                return jsonParams;
            }
        };
        postrequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        postrequest.setShouldCache(false);

        AppController.getInstance().addToRequestQueue(postrequest);
    }*/

    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {

        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }


    public void onRoutingSuccess(PolylineOptions mPolyOptions) {
        PolylineOptions polyoptions = new PolylineOptions();
        polyoptions.color(Color.BLUE);
        polyoptions.width(10);
        polyoptions.addAll(mPolyOptions.getPoints());
        googleMap.addPolyline(polyoptions);
    }


    //-----------------Move Back on  phone pressed  back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            // nothing
            return true;
        }
        return false;
    }

    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
//final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
// All location settings are satisfied. The client can initialize location
// requests here.
//...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
// Location settings are not satisfied. But could be fixed by showing the user
// a dialog.
                        try {
// Show the dialog by calling startResolutionForResult(),
// and check the result in onActivityResult().
                            status.startResolutionForResult(EndTrip.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
// Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
// Location settings are not satisfied. However, we have no way to fix the
// settings so we won't show the dialog.
//...
                        break;
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOCATION) {
            System.out.println("----------inside request location------------------");

            switch (resultCode) {
                case Activity.RESULT_OK: {
                    Toast.makeText(EndTrip.this, "Location enabled!", Toast.LENGTH_LONG).show();
                    break;
                }
                case Activity.RESULT_CANCELED: {
                    enableGpsService();
                    break;
                }
                default: {
                    break;
                }
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (progress > 95) {
            seekBar.setThumb(getResources().getDrawable(R.drawable.slidetounlock_arrow));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        Bt_slider.setVisibility(View.INVISIBLE);


    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

        //shimmer = new Shimmer();
        if (seekBar.getProgress() < 80) {
            seekBar.setProgress(0);
            sliderSeekBar.setBackgroundResource(R.drawable.red_slide_to_unlock_bg);
            Bt_slider.setVisibility(View.VISIBLE);
            Bt_slider.setText(getResources().getString(R.string.lbel_endtrip));
            //shimmer.start(Bt_slider); chaitanya
        } else if (seekBar.getProgress() > 90) {
            seekBar.setProgress(100);
            Bt_slider.setVisibility(View.VISIBLE);
            Bt_slider.setText(getResources().getString(R.string.lbel_endtrip));
            //shimmer.start(Bt_slider); chaitanya
            sliderSeekBar.setVisibility(View.VISIBLE);
            System.out.println("------------------sliding completed----------------");

            cd = new ConnectionDetector(EndTrip.this);
            isInternetPresent = cd.isConnectingToInternet();

            if (isInternetPresent) {
                PostRequest(ServiceConstant.endtrip_url);
                System.out.println("end------------------" + ServiceConstant.endtrip_url);
            } else {

                Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
            }
        }
    }


    //-----------------------Update current Location for notification  Post Request-----------------
    private void postRequest_UpdateProviderLocation(String Url) {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_rideid);
        jsonParams.put("latitude", Str_Latitude);
        jsonParams.put("longitude", Str_longitude);
        jsonParams.put("driver_id", driver_id);

        System.out.println("-------------Endtripride_id----------------" + Str_longitude);
        System.out.println("-------------Endtriplatitude----------------" + Str_Latitude);
        System.out.println("-------------Endtriplongitude----------------" + Str_longitude);

        System.out.println("-------------latlongupdate----------------" + Url);
        mRequest = new ServiceRequest(EndTrip.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                Log.e("updatelocation", response);

                System.out.println("-------------latlongupdate----------------" + response);
               // updateDriverOnMap(Double.parseDouble(Str_Latitude),Double.parseDouble(Str_longitude));

            }

            @Override
            public void onErrorListener() {

            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        System.gc();
        if (chat != null) {
            chat.close();
        }
        mHandler.removeCallbacks(mHandlerTask);
        // stopService(mServiceIntent);
        // Log.i("MAINACT","onDestroy!");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mHandlerTask);
        //stopService(mServiceIntent);
        ///mSensorService.onDestroy();
        Log.i("MAINACT","onDestroy!");
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }






    //static LatLngInterpolator latLngInterpolator = new LatLngInterpolator.Spherical();

   /* public static void updateMap(LatLng latLng) {
        MarkerAnimation.animateMarkerToICS(movingMarker, latLng, latLngInterpolator);
    }*/

    LatLngInterpolator mLatLngInterpolator;

    private void updateDriverOnMap(double lat_decimal, double lng_decimal) {

        LatLng latLng = new LatLng(lat_decimal, lng_decimal);

        /*if (mLatLngInterpolator == null) {
            mLatLngInterpolator = new LatLngInterpolator.Linear();
        }*/

        Log.d("tess", "inside run ");
        if (currentMarker != null) {
            Log.d("UPDATEDRIVERONMAP", "inside run IF");
           // moveVechile(latLng, currentMarker);

            if (isFirstPosition) {
                startPosition = marker.getPosition();
                currentMarker.setAnchor(0.5f, 0.5f);
                googleMap.moveCamera(CameraUpdateFactory
                        .newCameraPosition
                                (new CameraPosition.Builder()
                                        .target(startPosition)
                                        .zoom(15.5f)
                                        .build()));
                isFirstPosition = false;
            } else {
                endPosition = new LatLng(latLng.latitude, latLng.longitude);

                Log.d("dhasgdh", startPosition.latitude + "--" + endPosition.latitude + "--Check --" + startPosition.longitude + "--" + endPosition.longitude);

                if ((startPosition.latitude != endPosition.latitude) || (startPosition.longitude != endPosition.longitude)) {

                    Log.e("jhdhasd", "NOT SAME");
                    startBikeAnimation(startPosition, endPosition);

                } else {

                    Log.e("hdgahsdg", "SAMME");
                }
            }



        } else {

            Log.d("UPDATEDRIVERONMAP", "inside run ELSE");
           /* mLatLngInterpolator = new LatLngInterpolator.Linear();*/
            currentMarker = googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.car_icon))
                    .anchor(0.5f, 0.5f)
                    .flat(true));

            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(18).build();
            CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            googleMap.moveCamera(camUpdate);


            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    latLng, 15.5f));

          //  moveVechile(latLng, currentMarker);

        }

    }


    private void startBikeAnimation(final LatLng start, final LatLng end) {



        final Handler handler = new Handler();
        Log.i("Inside", "startBikeAnimation called...");

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(3000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                //LogMe.i(TAG, "Car Animation Started...");
                v = valueAnimator.getAnimatedFraction();
                lng = v * end.longitude + (1 - v)
                        * start.longitude;
                lat = v * end.latitude + (1 - v)
                        * start.latitude;

                LatLng newPos = new LatLng(lat, lng);
                currentMarker.setPosition(newPos);
                currentMarker.setAnchor(0.5f, 0.5f);
                currentMarker.setRotation(getBearing(start, end));

                // todo : Shihab > i can delay here
                googleMap.moveCamera(CameraUpdateFactory
                        .newCameraPosition
                                (new CameraPosition.Builder()
                                        .target(newPos)
                                        .zoom(15.5f)
                                        .build()));

                startPosition = currentMarker.getPosition();

                /*Polyline line = googleMap.addPolyline(new PolylineOptions().add(startPosition,endPosition)
                        .width(5).color(Color.BLUE));*/

            }

        });
        valueAnimator.start();
        firsttimepolyline = false;

        //to add new polyline
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                startlatlng = start;
                if (startlatlng != null) {
                    GetRouteTask getRoute = new GetRouteTask();
                    getRoute.execute();
                }
            }
        };
        handler.postDelayed(runnable, 3000);



    }





    //Method for finding bearing between two points
    private float getBearing(LatLng begin, LatLng end) {
        Log.e("getbearing_inside","bearing");
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);

        return 1f;

    }









}
