package com.indobytes.ridehubdriver;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.Hockeyapp.FragmentHockeyApp;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.indobytes.ridehubdriver.Pojo.ReferralPointsPojo;
import com.indobytes.ridehubdriver.Utils.ConnectionDetector;
import com.indobytes.ridehubdriver.Utils.SessionManager;
import com.indobytes.ridehubdriver.adapter.ReferralPointsAdapter;
import com.indobytes.ridehubdriver.widgets.CustomTextView;
import com.indobytes.ridehubdriver.widgets.PkDialog;
import com.special.ResideMenu.ResideMenu;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by user14 on 9/22/2015.
 */
public class ReferralPointsActivity extends FragmentHockeyApp {

    private View parentView;
    private ResideMenu resideMenu;
    ListView listView_referralpoints;
    CustomTextView refer_points_subtotal_textView;
    StringRequest postrequest;
    private SessionManager session;
    String driver_id = "", payid;
    private ArrayList<ReferralPointsPojo> referralpointslist;
    private ReferralPointsAdapter adapter;
    private Dialog dialog;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ActionBar actionBar;
    private TextView empty_Tv;
    private boolean show_progress_status = false;

    private ServiceRequest mRequest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.referral_points_activity, container, false);
        ActionBarActivity actionBarActivity = (ActionBarActivity) getActivity();
        actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.hide();
        initialize(parentView);
        parentView.findViewById(R.id.ham_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(resideMenu != null ){
                    resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
                }
            }
        });

        setUpViews();
        return parentView;
    }

    private void setUpViews() {
        NavigationDrawer parentActivity = (NavigationDrawer) getActivity();
        resideMenu = parentActivity.getResideMenu();
    }

     private void initialize(View rootview) {
        session = new SessionManager(getActivity());
         listView_referralpoints = (ListView) rootview.findViewById(R.id.listView_referralpoints);
         refer_points_subtotal_textView = (CustomTextView) rootview.findViewById(R.id.refer_points_subtotal_textView);
        empty_Tv = (TextView) rootview.findViewById(R.id.payment_no_textview);

        HashMap<String, String> user = session.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);


        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            PostRequest(ServiceConstant.referralpoints_url);
            System.out.println("Referral------------------" + ServiceConstant.referralpoints_url);
        } else {

            Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));

        }

    }

    //--------------Alert Method-----------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //-----------------------Code for payment details post request-----------------
    private void PostRequest(String Url) {
        dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------paymentdetails----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        System.out
                .println("--------------driver_id-------------------"
                        + driver_id);

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("REFERRALPOINTS", response);

                String status = "",   total_points = "",Str_response="";
                try {
                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");
                    total_points = object.getString("total_points");

                    Log.e("REFERRALPOINTSSTATUS", status+"-----"+total_points);
                    if (status.equalsIgnoreCase("1")){

                        JSONArray jarry = object.getJSONArray("data");
                        referralpointslist = new ArrayList<ReferralPointsPojo>();
                        System.out.println("ARRAYLENGTH--------" + jarry.length());
                        if (jarry.length() > 0) {
                            for (int i = 0; i<jarry.length(); i++) {
                                JSONObject jobject = jarry.getJSONObject(i);
                                ReferralPointsPojo item = new ReferralPointsPojo();
                                item.setReferrer_name(jobject.getString("referrer_name"));
                                item.setDate(jobject.getString("date"));
                                item.setRefer_from(jobject.getString("refer_from"));
                                item.setRefer_to(jobject.getString("refer_to"));
                                item.setType(jobject.getString("type"));
                                item.setRide_id(jobject.getString("ride_id"));
                                item.setTrans_type(jobject.getString("trans_type"));
                                item.setLevel(jobject.getString("level"));
                                item.setPoints(jobject.getString("points"));

                                System.out.println("referrer_name--------" + jobject.getString("referrer_name"));

                                referralpointslist.add(item);
                            }
                            refer_points_subtotal_textView.setText(total_points);
                          //  show_progress_status = true;

                        } else {
                          //  show_progress_status = false;
                        }

                    }else{
                        Str_response = object.getString("response");
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")){
                    adapter = new ReferralPointsAdapter(getActivity(), referralpointslist);
                    listView_referralpoints.setAdapter(adapter);
                    dialog.dismiss();

                    if (show_progress_status) {
                        empty_Tv.setVisibility(View.GONE);
                    } else {
                        empty_Tv.setVisibility(View.VISIBLE);
                        listView_referralpoints.setEmptyView(empty_Tv);
                    }

                }else{

                    Alert(getResources().getString(R.string.alert_sorry_label_title),Str_response);
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }

        });
    }





    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {

        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }


}


