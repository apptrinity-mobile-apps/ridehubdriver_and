package com.indobytes.ridehubdriver;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.Hockeyapp.ActivityHockeyApp;
import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceManager;
import com.app.service.ServiceRequest;
import com.app.xmpp.ChatingService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indobytes.ridehubdriver.Utils.GPSTracker;
import com.indobytes.ridehubdriver.Utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


/**
 */
public class DriverMapActivity extends ActivityHockeyApp implements View.OnClickListener, LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    // Google Map
    private GoogleMap googleMap;
    private Location location = null;
    public static Location myLocation;
    private SessionManager session;
    private Dialog dialog;
    private Marker currentMarker;
    private RelativeLayout Rl_layout_available_status;
    private String Str_rideId = "";
    private String driver_id = "";
    private boolean isGpsEnabled;

    private ServiceRequest mRequest;

    private BroadcastReceiver receiver;
    private GPSTracker gps;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private PendingResult<LocationSettingsResult> result;
    private final static int REQUEST_LOCATION = 199;
    private Handler mapHandler = new Handler();
    Marker drivermarker;
    private Runnable mapRunnable = new Runnable() {
        @Override
        public void run() {
            gps = new GPSTracker(DriverMapActivity.this);
            if (gps != null && gps.canGetLocation()) {
                System.out.println("======map handler===========");
                postRequest(ServiceConstant.UPDATE_CURRENT_LOCATION);
            } else {
                enableGpsService();
            }
            mapHandler.postDelayed(this, 30000);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.roadmap);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.roadmap);
        } else {
            setContentView(R.layout.roadmap);
        }


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        session = new SessionManager(DriverMapActivity.this);
        gps = new GPSTracker(this);
        HashMap<String, String> user = session.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
        ImageButton refresh_button = (ImageButton) findViewById(R.id.refresh);
        refresh_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(DriverMapActivity.this, DriverMapActivity.class);
                finish();
                startActivity(i);
            }
        });
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.finish.canceltrip.DriverMapActivity");
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("com.finish.canceltrip.DriverMapActivity")) {
                    Rl_layout_available_status.setVisibility(View.GONE);
                }
            }
        };
        try {
            registerReceiver(receiver, filter);
        } catch (Exception e) {

        }
        final Button goOffline = (Button) findViewById(R.id.go_offline);
        Rl_layout_available_status = (RelativeLayout) findViewById(R.id.layout_available_status);
        Rl_layout_available_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DriverMapActivity.this, TripSummaryDetail.class);
                intent.putExtra("ride_id", Str_rideId);
                System.out.println("StrRideID---------" + Str_rideId);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        goOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goOffLine();
            }

        });

        try {
            session = new SessionManager(this);
            setLocationRequest();
            buildGoogleApiClient();
            initilizeMap();
        } catch (Exception e) {
        }
        initView();


        if (gps != null && gps.canGetLocation() && gps.isgpsenabled()) {
        } else {
            enableGpsService();
            //showGpsDisableDialog(getResources().getString(R.string.label_gps_textview));
        }
        mapHandler.post(mapRunnable);
    }


    @Override
    public void onBackPressed() {
        //   super.onBackPressed();
        showBackPressedDialog();
    }

    private void showBackPressedDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage(R.string.label_sure_go_offline)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        Dialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapHandler.removeCallbacks(mapRunnable);
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapRunnable.run();
        ChatingService.startDriverAction(DriverMapActivity.this);
        startLocationUpdates();
        if(myLocation==null)
        {
            myLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
        }
    }

    private void initView() {
    }

    public void showDialog(String message) {
        dialog = new Dialog(this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    public void dismissDialog() {
        dialog.dismiss();
    }

    private void postRequest(final String url) {
        if (myLocation != null) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            HashMap<String, String> userDetails = session.getUserDetails();
            String driverId = userDetails.get("driverid");
            System.out.println("driverId-------------" + driverId);
            System.out.println("latitude-------------" + myLocation.getLatitude());
            System.out.println("longitude-------------" + myLocation.getLongitude());
            jsonParams.put("driver_id", "" + driverId);
            jsonParams.put("latitude", "" + myLocation.getLatitude());
            jsonParams.put("longitude", "" + myLocation.getLongitude());
            ServiceManager manager = new ServiceManager(DriverMapActivity.this, mServiceListener);
            manager.makeServiceRequest(url, Request.Method.POST, jsonParams);
        } else {
//            Toast.makeText(DriverMapActivity.this, "Location Not Update", Toast.LENGTH_SHORT).show();
        }
    }

    private ServiceManager.ServiceListener mServiceListener = new ServiceManager.ServiceListener() {
        private String Str_status = ""
                ,
                Str_availablestaus = ""
                ,
                Str_message = "";

        @Override
        public void onCompleteListener(Object object) {
            try {
                String response = (String) object;
                JSONObject jobject = new JSONObject(response);

                Str_status = jobject.getString("status");
                if ("1".equalsIgnoreCase(Str_status)) {
                    JSONObject jobject2 = jobject.getJSONObject("response");
                    Str_availablestaus = jobject2.getString("availability");
                    Str_message = jobject2.getString("message");
                    Str_rideId = jobject2.getString("ride_id");
                    System.out.println("rideIDDresponse----------" + Str_rideId);
                    System.out.println("online----------" + response);
                    if (Str_availablestaus.equalsIgnoreCase("Unavailable")) {
                        Rl_layout_available_status.setVisibility(View.VISIBLE);
                    } else {
                        Rl_layout_available_status.setVisibility(View.GONE);
                    }
                    showVerifyStatus(jobject2);
                }
            } catch (Exception e) {
            }
        }

        @Override
        public void onErrorListener(Object obj) {

        }
    };





    private void showVerifyStatus(JSONObject object){
        try {
           String verify_status = object.getString("verify_status");
            if("No".equalsIgnoreCase(verify_status)){
                findViewById(R.id.layout_verify_status).setVisibility(View.VISIBLE);
            }else{
                findViewById(R.id.layout_verify_status).setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            findViewById(R.id.layout_verify_status).setVisibility(View.GONE);
        }
    }


    private void initilizeMap() {
        // latitude and longitude
        /// gps = new GPSTracker(this);
        double latitude;
        double longitude;
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();
            myLocation = googleMap.getMyLocation();
            googleMap.setMapType(R.raw.map_silver_json);


            googleMap.setMyLocationEnabled(false);
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.getUiSettings().setCompassEnabled(false);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            googleMap.getUiSettings().setZoomGesturesEnabled(true);
            googleMap.getUiSettings().setAllGesturesEnabled(false);

            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public void goOffLine() {
        DashBoardDriver.isOnline = false;
        showDialog("");
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        HashMap<String, String> userDetails = session.getUserDetails();
        HashMap<String, String> LoginDetails = session.getSessionlogin_details();

        String loginid = LoginDetails.get("loginid");
        String logintime = LoginDetails.get("logintime");
        System.out.println("loginid-------------" + loginid);
        System.out.println("logintime-------------" + logintime);

        String driverId = userDetails.get("driverid");
        jsonParams.put("driver_id", "" + driverId);
        jsonParams.put("availability", "" + "No");
        jsonParams.put("login_id", "" + loginid);
        jsonParams.put("login_time", "" + logintime);
        System.out.println("availability-------------" + "No");
        System.out.println("offline driver_id-------------" + driverId);
        ServiceManager manager = new ServiceManager(this, updateAvailabilityServiceListener);
//        ChatingService.closeConnection();
        manager.makeServiceRequest(ServiceConstant.UPDATE_AVAILABILITY, Request.Method.POST, jsonParams);
    }

    private ServiceManager.ServiceListener updateAvailabilityServiceListener = new ServiceManager.ServiceListener() {
        @Override
        public void onCompleteListener(Object object) {
            try {
                dismissDialog();
                String response = (String) object;
                System.out.println("goofflineresponse---------" + response);


                JSONObject object1=new JSONObject(response);
                if(object1.length()>0)
                {
                    String status=object1.getString("status");
                    if(status.equalsIgnoreCase("1"))
                    {
                        finish();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onErrorListener(Object obj) {
            dismissDialog();
            finish();
        }
    };

    private void setLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    @Override
    public void onConnected(Bundle bundle) {
        if (gps != null && gps.canGetLocation() && gps.isgpsenabled()) {
        }
        myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        if (myLocation != null) {

            drivermarker= googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.main_map_marker)));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()),
                    17));
            //MarkerOptions marker = new MarkerOptions();
            //marker.position(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
            //marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.red_car));
           // if(currentMarker != null){
            //    currentMarker.remove();
           // }
           // currentMarker = googleMap.addMarker(marker);
            postRequest(ServiceConstant.UPDATE_CURRENT_LOCATION);
            System.out.println("online------------------" + ServiceConstant.UPDATE_CURRENT_LOCATION);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }


    @Override
    public void onLocationChanged(Location location) {

        this.myLocation = location;
        if (myLocation != null) {
            try {
                LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                if (drivermarker != null) {
                    drivermarker.remove();
                }
                drivermarker = googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.main_map_marker)));
                float zoom = googleMap.getCameraPosition().zoom;
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
                CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                googleMap.moveCamera(camUpdate);

            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(DriverMapActivity.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        enableGpsService();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
// Unregister the logout receiver
        mapHandler.removeCallbacks(mapRunnable);
        unregisterReceiver(receiver);
        super.onDestroy();
    }



}



