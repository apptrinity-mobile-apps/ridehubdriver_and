package com.app.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.xmpp.ChatConfigurationBuilder;
import com.indobytes.ridehubdriver.EndTrip;
import com.indobytes.ridehubdriver.R;
import com.indobytes.ridehubdriver.Utils.ConnectionDetector;
import com.indobytes.ridehubdriver.Utils.GPSTracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by fabio on 30/01/2016.
 */
public class SensorService extends Service {

    public int counter=0;
    GPSTracker gps;
    ChatConfigurationBuilder builder;
    private String Str_Latitude = "", Str_longitude = "",Str_rideid= "",driver_id="",chatID="";
    private ServiceRequest mRequest;
    public SensorService(Context applicationContext) {
        super();
        Log.i("HERE", "here I am!");
    }

    public SensorService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        gps = new GPSTracker(getApplicationContext());
        builder = new ChatConfigurationBuilder(this);

        Str_rideid=(String) intent.getExtras().get("rideid");
        driver_id=(String) intent.getExtras().get("driverid");
        chatID=(String) intent.getExtras().get("chatID");

        Log.d("receive service",Str_rideid+"-"+driver_id);

        startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        Intent broadcastIntent = new Intent("com.app.service.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);
        stoptimertask();
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime=0;
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 1000, 10000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  "+ (counter++));




                    if (gps != null && gps.canGetLocation() && gps.isgpsenabled()) {

                        Str_Latitude = String.valueOf(gps.getLatitude());
                        Str_longitude = String.valueOf(gps.getLongitude());


                        try {
                            sendLocationToUser(gps.getLocation());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        postRequest_UpdateProviderLocation(ServiceConstant.UPDATE_CURRENT_LOCATION);
                    }



                       /* Intent intent = new Intent(getApplicationContext(), GoogleService.class);
                        startService(intent);*/



            }
        };
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //-----------------------Update current Location for notification  Post Request-----------------
    private void postRequest_UpdateProviderLocation(String Url) {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_rideid);
        jsonParams.put("latitude", Str_Latitude);
        jsonParams.put("longitude", Str_longitude);
        jsonParams.put("driver_id", driver_id);

        System.out.println("-------------service Endtripride_id----------------" + Str_longitude);
        System.out.println("-------------service Endtriplatitude----------------" + Str_Latitude);
        System.out.println("-------------service Endtriplongitude----------------" + Str_longitude);

        System.out.println("-------------service latlongupdate----------------" + Url);
        mRequest = new ServiceRequest(getApplicationContext());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                Log.e("service updatelocation", response);

                System.out.println("-------------service latlongupdate----------------" + response);

            }

            @Override
            public void onErrorListener() {

            }
        });
    }

    JSONObject job;
    private void sendLocationToUser(Location location) throws JSONException {



        String sendLat = Double.valueOf(location.getLatitude()).toString();
        String sendLng = Double.valueOf(location.getLongitude()).toString();

        System.out.println("endtripchatID--------------"+chatID+"--"+sendLat+"--"+sendLat+"--"+Str_rideid);
        if (job == null) {
            job = new JSONObject();
        }
        job.put("action", "driver_loc");
        job.put("latitude", sendLat);
        job.put("longitude", sendLng);
        job.put("device_type","android");
        job.put("background","no");
        job.put("ride_id", Str_rideid);
        builder.sendMessage(chatID, job.toString());


       /* String sToID = ContinuousRequestAdapter.userID + "@" + ServiceConstant.XMPP_SERVICE_NAME;
        try {
            if(chat  != null){
                chat.sendMessage(job.toString());
            }else{
                chat = ChatingService.createChat(sToID);
                chat.sendMessage(job.toString());
            }
        } catch (SmackException.NotConnectedException e) {
            try {
                chat = ChatingService.createChat(sToID);
                chat.sendMessage(job.toString());
            }catch (SmackException.NotConnectedException e1){
                Toast.makeText(this,"Not Able to send data to the user Network Error",Toast.LENGTH_SHORT).show();
            }
        }*/

    }


}